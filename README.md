# Homebrew-Mnem

This is a Homebrew tap for installing [Mnem](https://gitlab.com/dmfay/mnem).

```
brew tap dmfay/mnem git@gitlab.com:dmfay/homebrew-mnem.git
brew install dmfay/mnem/mnem
```
