class Mnem < Formula
  homepage "https://gitlab.com/dmfay/mnem"
  url "https://gitlab.com/dmfay/mnem/-/archive/main/mnem-main.tar.gz"
  sha256 "c2b12fcf7598f8ea5606ba4ca7b2fa822842f70f5e63b27f99aabe67f5a8f747"
  version "0.1.0"

  head "https://gitlab.com/dmfay/mnem.git"
  
  bottle :unneeded

  depends_on "rust" => :build

  def install
    system "cargo", "install", *std_cargo_args
  end

end
